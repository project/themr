var checkReadyState = setInterval(() => {
  if (document.readyState === "complete") {
    clearInterval(checkReadyState);
    (function ($, drupalSettings) {
      $('#themr-ai-chat-text').keyup(function (e) {
        if (e.keyCode == 13) {
          $(this).attr('disabled', 'disabled').css('background-color', '#ccc');
          $.post(drupalSettings.path.baseUrl + 'admin/themr_chat/response', {
            'instruction': $(this).val(),
            'markup': themrGetFullElement(),
            'css': themrGetComputedStyles(),
          }, function (result) {
            $('#themr-ai-chat-text').attr('disabled', false).css('background-color', '#fff');
            if (result.status) {
              for (let x in result.values) {
                console.log('test', x, result.values[x])
                themrRerenderElement(x, result.values[x]);
              }
              $('#themr-ai-chat-text').val('');
              $('#themr-ai-chat-text').trigger('focus');
              return false;
            }
          })
          return false;
        }
      });
    })(jQuery, drupalSettings);
  }
}, 100);

(function ($, Drupal, drupalSettings) {
  $(document).ready(function () {

  });
})(jQuery, Drupal, drupalSettings);
