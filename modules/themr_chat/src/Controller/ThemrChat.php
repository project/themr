<?php

namespace Drupal\themr_chat\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use OpenAI\Client;
use OpenAI\Exceptions\ErrorException;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Outputs the CSS.
 */
class ThemrChat extends ControllerBase {


  /**
   * The OpenAI client.
   */
  protected Client $client;

  /**
   * Constructs a new CampaignListBuilder object.
   *
   * @param \OpenAI\Client $client
   *   The OpenAI client.
   */
  public function __construct(Client $client,)
  {
    $this->client = $client;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('openai.client'),
    );
  }
  /**
   * Output the CSS.
   */
  public function response(Request $request) {
    // Make sure all exists.
    $instruction = $request->get('instruction');
    $css = $request->get('css');
    $markup = $request->get('markup');
    // Make prompt.
    if ($instruction && $css && $markup) {
      $prompt = "Given the following HTML and CSS of a input submit HTML element, change the CSS only according to the instructions. Answer in JSON format with only the changes, additions and removals of properties that needs to change. If it need to be removed only answer with the property.\n\n";
      $prompt .= "Do not include any explanations, only provide a RFC8259 compliant JSON response following this format without deviation.\n{\"css property\": \"css value\", \"css property\": \"css value\"}.\n\n";
      $prompt .= "Example for wanting to set a background color instead of an image: {\"background-color\": \"#c4e2e4\", \"background-image\": \"\"}\n\n";
      $prompt .= "HTML:\n--------------------------------\n$markup\n------------------------------\n\n";
      $prompt .= "CSS:\n--------------------------------\n$css\n------------------------------\n\n";
      $prompt .= "Instruction:\n--------------------------------\n$instruction\n------------------------------\n\n";
      $response = $this->cssRequest($prompt);
      if (!empty($response['choices'][0]['message']['content'])) {
        $values = json_decode($response['choices'][0]['message']['content'], TRUE);
        if (is_array($values) && count($values)) {
          return new JsonResponse([
            'status' => TRUE,
            'values' => $values,
          ]);
        }
      }
    }

    return new JsonResponse(['status' => FALSE]);
  }

  /**
   * Generate the OpenAI Chat request.
   *
   * @param string $prompt
   *   The prompt.
   *
   * @return string
   *   The response.
   */
  protected function cssRequest($prompt) {
    $messages[] = [
      'role' => 'system',
      'content' => 'You are a CSS frontend expert asking direct code questions about CSS in JSON format.',
    ];
    $messages[] = [
      'role' => 'user',
      'content' => trim($prompt),
    ];
    try {
      $response = $this->client->chat()->create([
        'model' => 'gpt-3.5-turbo',
        'messages' => $messages,
      ]);
    } catch (ErrorException $e) {
      return $e->getMessage();
    }

    $result = $response->toArray();

    return $result;
  }
}
