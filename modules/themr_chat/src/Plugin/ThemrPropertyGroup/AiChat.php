<?php

namespace Drupal\themr_chat\Plugin\ThemrPropertyGroup;

use Drupal\themr\PluginInterfaces\ThemrPropertyGroupInterface;

/**
 * The AI Chat group.
 *
 * @ThemrPropertyGroup(
 *   id = "ai_chat",
 *   title = @Translation("AI Chat"),
 *   open = TRUE,
 *   weight = -1,
 *   tab = "ai"
 * )
 */
class AiChat implements ThemrPropertyGroupInterface {
}
