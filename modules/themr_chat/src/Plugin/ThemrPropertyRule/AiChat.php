<?php

namespace Drupal\themr_chat\Plugin\ThemrPropertyRule;

use Drupal\themr\PluginInterfaces\ThemrPropertyRuleInterface;
use Drupal\Core\Form\FormStateInterface;

/**
 * The AI Chat rule.
 *
 * @ThemrPropertyRule(
 *   id = "ai_chat",
 *   title = @Translation("AI Chat"),
 *   group = "ai_chat",
 *   weight = 0
 * )
 */
class AiChat implements ThemrPropertyRuleInterface {

  /**
   * {@inheritDoc}
   */
  public function addElement($form, FormStateInterface $form_state) {
    $element['ai_chat'] = [
      '#type' => 'textfield',
      '#title' => 'AI Instructor',
      '#description' => 'Write a human instruction on how to change things',
      '#attributes' => [
        'id' => 'themr-ai-chat-text',
      ],
    ];

    return $element;
  }

  /**
   * {@inheritDoc}
   */
  public function excludeElements() {
    return [
      'img',
    ];
  }

  /**
   * {@inheritDoc}
   */
  public function triggerEvents() {
    return [];
  }

  /**
   * {@inheritDoc}
   */
  public function addLibrary() {
    return [
      'themr_chat/themr_chat'
    ];
  }
}
