<?php

namespace Drupal\themr_chat\Plugin\ThemrTabs;

use Drupal\themr\PluginInterfaces\ThemrTabsInterface;

/**
 * The AI tab.
 *
 * @ThemrTabs(
 *   id = "ai",
 *   title = @Translation("AI"),
 *   weight = 0
 * )
 */
class Ai implements ThemrTabsInterface {
}
