<?php

namespace Drupal\themr\PluginManager;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;

/**
 * Provides an Themr Suggestion Element Rule process plugin manager.
 *
 * @see \Drupal\themr\Annotation\ThemrSuggestionElementRule
 * @see \Drupal\themr\PluginInterfaces\ThemrSuggestionElementRuleInterface
 * @see plugin_api
 */
class ThemrSuggestionElementManager extends DefaultPluginManager {

  /**
   * Constructs a ThemrElementManager object.
   *
   * @param \Traversable $namespaces
   *   An object that implements \Traversable which contains the root paths
   *   keyed by the corresponding namespace to look for plugin implementations.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   Cache backend instance to use.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler to invoke the alter hook with.
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler)
  {
    parent::__construct(
      'Plugin/ThemrSuggestionElementRule',
      $namespaces,
      $module_handler,
      'Drupal\themr\PluginInterfaces\ThemrSuggestionElementRuleInterface',
      'Drupal\themr\Annotation\ThemrSuggestionElementRule'
    );
    $this->alterInfo('themr_suggestion_element_rule');
    $this->setCacheBackend($cache_backend, 'themr_suggestion_element_rule_plugins');
  }
}
