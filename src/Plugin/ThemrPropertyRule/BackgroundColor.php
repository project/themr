<?php

namespace Drupal\themr\Plugin\ThemrPropertyRule;

use Drupal\themr\PluginInterfaces\ThemrPropertyRuleInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * The background color rule.
 *
 * @ThemrPropertyRule(
 *   id = "background_color",
 *   title = @Translation("Background Color"),
 *   css_property = "background-color",
 *   group = "background",
 *   weight = 0
 * )
 */
class BackgroundColor implements ThemrPropertyRuleInterface, ContainerFactoryPluginInterface {

  /**
   * Constructor.
   */
  public function __construct() {
  }

  /**
   * {@inheritDoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static();
  }

  /**
   * {@inheritDoc}
   */
  public function addElement($form, FormStateInterface $form_state) {
    $element['background_color'] = [
      '#type' => 'color',
      '#title' => t('Background Color'),
      '#default_value' => '#ffffff',
      '#attributes' => [
        'id' => 'background_color',
      ],
    ];
    return $element;
  }

  /**
   * {@inheritDoc}
   */
  public function excludeElements() {
    return [
      'img',
    ];
  }

  /**
   * {@inheritDoc}
   */
  public function triggerEvents() {
    return [
      'input',
      'change',
    ];
  }

  /**
   * {@inheritDoc}
   */
  public function addLibrary() {
  }

}
