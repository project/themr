<?php

namespace Drupal\themr\Plugin\ThemrPropertyRule;

use Drupal\themr\PluginInterfaces\ThemrPropertyRuleInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * The color rule.
 *
 * @ThemrPropertyRule(
 *   id = "color",
 *   title = @Translation("Color"),
 *   css_property = "color",
 *   group = "font",
 *   weight = 0
 * )
 */
class Color implements ThemrPropertyRuleInterface, ContainerFactoryPluginInterface {

  /**
   * Constructor.
   */
  public function __construct() {
  }

  /**
   * {@inheritDoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static();
  }

  /**
   * {@inheritDoc}
   */
  public function addElement($form, FormStateInterface $form_state) {
    $element['color'] = [
      '#type' => 'color',
      '#title' => t('Color'),
      '#default_value' => '#000000',
      '#attributes' => [
        'id' => 'color',
      ],
    ];
    return $element;
  }

  /**
   * {@inheritDoc}
   */
  public function excludeElements() {
    return [
      'img',
    ];
  }

  /**
   * {@inheritDoc}
   */
  public function triggerEvents() {
    return [
      'input',
      'change',
    ];
  }

  /**
   * {@inheritDoc}
   */
  public function addLibrary() {
  }
}
