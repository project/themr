<?php

namespace Drupal\themr\Plugin\ThemrPropertyRule;

use Drupal\themr\PluginInterfaces\ThemrPropertyRuleInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * The background image rule.
 *
 * @ThemrPropertyRule(
 *   id = "background_image_gradient",
 *   title = @Translation("Background Image (Gradient)"),
 *   css_property = "background-image",
 *   group = "background",
 *   weight = 0
 * )
 */
class BackgroundImageGradient implements ThemrPropertyRuleInterface, ContainerFactoryPluginInterface {

  /**
   * Constructor.
   */
  public function __construct() {
  }

  /**
   * {@inheritDoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static();
  }

  /**
   * {@inheritDoc}
   */
  public function addElement($form, FormStateInterface $form_state) {
    $element['background_image_gradient_type'] = [
      '#title' => 'Background Gradient',
      '#type' => 'select',
      '#options' => [
        'linear-gradient' => t('Linear Gradient'),
      ],
      '#attributes' => [
        'id' => 'background-image-gradient-type',
      ],
    ];
    $element['background_image_gradient_direction'] = [
      '#title' => 'Angle',
      '#type' => 'range',
      '#min' => 0,
      '#max' => 359,
      '#default_value' => 0,
      '#attributes' => [
        'id' => 'background-image-gradient-direction',
      ],
    ];
    $element['background_image_gradient'] = [
      '#type' => 'hidden',
      '#attributes' => [
        'id' => 'background-image-gradient',
      ],
    ];
    $element['background_gradient_picker'] = [
      '#markup' => '<div id="background-gradient-picker"></div>'
    ];
    $element['background_gradient_preview'] = [
      '#markup' => '<div id="background-gradient-preview"></div>'
    ];
    return $element;
  }

  /**
   * {@inheritDoc}
   */
  public function excludeElements() {
    return [
      'img',
    ];
  }

  /**
   * {@inheritDoc}
   */
  public function triggerEvents() {
    return [
      'input',
      'change',
    ];
  }

  /**
   * {@inheritDoc}
   */
  public function addLibrary() {
    return [
      'themr/themr_background_gradient',
    ];
  }
}
