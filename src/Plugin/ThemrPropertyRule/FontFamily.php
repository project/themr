<?php

namespace Drupal\themr\Plugin\ThemrPropertyRule;

use Drupal\themr\PluginInterfaces\ThemrPropertyRuleInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Link;
use Drupal\Core\Url;

/**
 * The font size rule.
 *
 * @ThemrPropertyRule(
 *   id = "font_family",
 *   title = @Translation("Font Family"),
 *   css_property = "font-family",
 *   group = "font",
 *   weight = 0
 * )
 */
class FontFamily implements ThemrPropertyRuleInterface {

  /**
   * {@inheritDoc}
   */
  public function addElement($form, FormStateInterface $form_state) {
    $storage = \Drupal::entityTypeManager()->getStorage('font');
    $query = $storage->getQuery()->accessCheck(TRUE);
    $fonts = [];
    foreach ($query->execute() as $id) {
      $font = $storage->load($id);
      if ($font->isActivated()) {
        $fonts[$font->css_family->value] =  $font->label();
      }
    }
    $element['font_family'] = [
      '#type' => 'select',
      '#title' => t('Font Family'),
      '#options' => $fonts,
      '#attributes' => [
        'id' => 'font_family',
      ],
      '#description' => t('Add more fonts @here', [
        '@here' => Link::fromTextAndUrl(t('here'), Url::fromRoute('entity.font.collection'))->toString(),
      ]),
    ];
    return $element;
  }

  /**
   * {@inheritDoc}
   */
  public function excludeElements()
  {
    return [
      'img',
    ];
  }

  /**
   * {@inheritDoc}
   */
  public function triggerEvents()
  {
    return [
      'change',
    ];
  }

  /**
   * {@inheritDoc}
   */
  public function addLibrary() {
  }
}
