<?php

namespace Drupal\themr\Plugin\ThemrPropertyRule;

use Drupal\themr\PluginInterfaces\ThemrPropertyRuleInterface;
use Drupal\Core\Form\FormStateInterface;

/**
 * The font size rule.
 *
 * @ThemrPropertyRule(
 *   id = "padding",
 *   title = @Translation("Padding"),
 *   group = "spacing",
 *   suffix = "rem",
 *   weight = 0
 * )
 */
class Padding implements ThemrPropertyRuleInterface {

  /**
   * {@inheritDoc}
   */
  public function addElement($form, FormStateInterface $form_state) {
    $element['padding'] = [
      '#type' => 'range',
      '#title' => t('Padding (REM)'),
      '#min' => 0.01,
      '#max' => 10,
      '#default_value' => 0,
      '#step' => 0.01,
      '#attributes' => [
        'id' => 'padding',
      ],
    ];

    $element['padding_per_side'] = [
      '#type' => 'checkbox',
      '#title' => t('Padding Per Side'),
      '#attributes' => [
        'id' => 'padding_per_side',
      ],
      '#weight' => -1,
    ];

    $element['padding_left'] = [
      '#type' => 'range',
      '#title' => t('Padding Left (REM)'),
      '#min' => 0.01,
      '#max' => 10,
      '#default_value' => 0,
      '#step' => 0.01,
      '#attributes' => [
        'id' => 'padding_left',
      ],
    ];

    $element['padding_right'] = [
      '#type' => 'range',
      '#title' => t('Padding Right (REM)'),
      '#min' => 0.01,
      '#max' => 10,
      '#default_value' => 0,
      '#step' => 0.01,
      '#attributes' => [
        'id' => 'padding_right',
      ],
    ];

    $element['padding_top'] = [
      '#type' => 'range',
      '#title' => t('Padding Top (REM)'),
      '#min' => 0.01,
      '#max' => 10,
      '#default_value' => 0,
      '#step' => 0.01,
      '#attributes' => [
        'id' => 'padding_top',
      ],
    ];

    $element['padding_bottom'] = [
      '#type' => 'range',
      '#title' => t('Padding Bottom (REM)'),
      '#min' => 0.01,
      '#max' => 10,
      '#default_value' => 0,
      '#step' => 0.01,
      '#attributes' => [
        'id' => 'padding_bottom',
      ],
    ];

    return $element;
  }

  /**
   * {@inheritDoc}
   */
  public function excludeElements() {
    return [
      'img',
    ];
  }

  /**
   * {@inheritDoc}
   */
  public function triggerEvents() {
    return [
      'input',
      'change',
    ];
  }

  /**
   * {@inheritDoc}
   */
  public function addLibrary() {
    return [
      'themr/themr_padding'
    ];
  }
}
