<?php

namespace Drupal\themr\Plugin\ThemrPropertyRule;

use Drupal\themr\PluginInterfaces\ThemrPropertyRuleInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * The background image rule.
 *
 * @ThemrPropertyRule(
 *   id = "background_image",
 *   title = @Translation("Background Image (Media)"),
 *   css_property = "background-image",
 *   group = "background",
 *   weight = 0
 * )
 */
class BackgroundImage implements ThemrPropertyRuleInterface, ContainerFactoryPluginInterface {

  /**
   * Constructor.
   */
  public function __construct()
  {
  }

  /**
   * {@inheritDoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition)
  {
    return new static();
  }

  /**
   * {@inheritDoc}
   */
  public function addElement($form, FormStateInterface $form_state) {
    $element['background_image'] = [
      '#type'          => 'entity_autocomplete',
      '#target_type'   => 'media',
      '#title' => t('Background Image (Media)'),
      '#attributes' => [
        'id' => 'background_image',
      ],
    ];
    return $element;
  }

  /**
   * {@inheritDoc}
   */
  public function excludeElements() {
    return [
      'img',
    ];
  }

  /**
   * {@inheritDoc}
   */
  public function triggerEvents() {
    return [
      'input',
      'change',
    ];
  }

  /**
   * {@inheritDoc}
   */
  public function addLibrary() {
  }
}
