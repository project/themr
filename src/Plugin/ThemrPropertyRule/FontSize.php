<?php

namespace Drupal\themr\Plugin\ThemrPropertyRule;

use Drupal\themr\PluginInterfaces\ThemrPropertyRuleInterface;
use Drupal\Core\Form\FormStateInterface;

/**
 * The font size rule.
 *
 * @ThemrPropertyRule(
 *   id = "font_size",
 *   title = @Translation("Font Size"),
 *   css_property = "font-size",
 *   group = "font",
 *   suffix = "rem",
 *   weight = 0
 * )
 */
class FontSize implements ThemrPropertyRuleInterface {

  /**
   * {@inheritDoc}
   */
  public function addElement($form, FormStateInterface $form_state) {
    $element['font_size'] = [
      '#type' => 'range',
      '#title' => t('Font Size (REM)'),
      '#min' => 0.05,
      '#max' => 5,
      '#default_value' => 1,
      '#step' => 0.05,
      '#attributes' => [
        'id' => 'font_size',
      ],
    ];
    return $element;
  }

  /**
   * {@inheritDoc}
   */
  public function excludeElements() {
    return [
      'img',
    ];
  }

  /**
   * {@inheritDoc}
   */
  public function triggerEvents() {
    return [
      'change',
    ];
  }

  /**
   * {@inheritDoc}
   */
  public function addLibrary() {
  }
}
