<?php

namespace Drupal\themr\Plugin\Block;

use Drupal\Core\Block\BlockBase;

/**
 * Provides a themr css injector block.
 *
 * @Block(
 *   id = "themr_css_injector",
 *   admin_label = @Translation("Themr CSS Injector"),
 *   category = @Translation("Custom")
 * )
 */
class CssInjectorBlock extends BlockBase {

  public function getCacheMaxAge() {
    return 0;
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $libraries = [
      'themr/themr_widget',
    ];
    $form = \Drupal::formBuilder()->getForm('Drupal\themr\Form\CssEditor');
    $inlineJs = [];
    foreach (\Drupal::service('plugin.manager.element_manager')->getDefinitions() as $element) {
      $inlineJs['allowed_elements'][$element['element']] = $element['title'];
    }
    $propertyManager = \Drupal::service('plugin.manager.property_manager');
    foreach ($propertyManager->getDefinitions() as $property) {
      $propertyRule = $propertyManager->createInstance($property['id']);
      if (!empty($propertyRule->addLibrary())) {
        $libraries = array_merge($libraries, $propertyRule->addLibrary());
      }
      if (isset($property['css_property'])) {
        $inlineJs['properties'][$property['id']] = [
          'property' => $property['css_property'],
          'selector' => '#' . $property['id'],
          'prefix' => $property['prefix'] ?? '',
          'suffix' => $property['suffix'] ?? '',
          'triggerEvents' => $propertyRule->triggerEvents(),
        ];
      }
    }

    // No block title by default.
    $this->configuration['admin_label'] = '';
    $this->configuration['label_display'] = FALSE;
    $cssFile = '/styles/themr_css.css';
    $render['content'] = [
      '#type' => 'inline_template',
      '#template' => '<link rel="stylesheet" media="all" href="{{ css_file }}" /><div id="themr-editor" class="themr-admin">{{ form }}</div>',
      '#context' => [
        'css_file' => $cssFile,
        'form' => $form,
      ],
      '#attached' => [
        'library' => $libraries,
        'drupalSettings' => [
          'themr' => $inlineJs,
        ],
      ]
    ];
    return $render;
  }

}
