<?php

namespace Drupal\themr\Plugin\ThemrPropertyGroup;

use Drupal\themr\PluginInterfaces\ThemrPropertyGroupInterface;

/**
 * The font rule.
 *
 * @ThemrPropertyGroup(
 *   id = "font",
 *   title = @Translation("Font"),
 *   open = TRUE,
 *   weight = 0,
 *   tab = "simple"
 * )
 */
class Font implements ThemrPropertyGroupInterface {
}
