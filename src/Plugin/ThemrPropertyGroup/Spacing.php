<?php

namespace Drupal\themr\Plugin\ThemrPropertyGroup;

use Drupal\themr\PluginInterfaces\ThemrPropertyGroupInterface;

/**
 * The background color rule.
 *
 * @ThemrPropertyGroup(
 *   id = "spacing",
 *   title = @Translation("Spacing"),
 *   open = FALSE,
 *   weight = 1,
 *   tab = "simple"
 * )
 */
class Spacing implements ThemrPropertyGroupInterface {
}
