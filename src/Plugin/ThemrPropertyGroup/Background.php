<?php

namespace Drupal\themr\Plugin\ThemrPropertyGroup;

use Drupal\themr\PluginInterfaces\ThemrPropertyGroupInterface;

/**
 * The background color rule.
 *
 * @ThemrPropertyGroup(
 *   id = "background",
 *   title = @Translation("Background"),
 *   open = TRUE,
 *   weight = 0,
 *   tab = "simple"
 * )
 */
class Background implements ThemrPropertyGroupInterface {
}
