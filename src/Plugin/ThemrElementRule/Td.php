<?php

namespace Drupal\themr\Plugin\ThemrElementRule;

use Drupal\themr\PluginInterfaces\ThemrElementRuleInterface;

/**
 * The background color rule.
 *
 * @ThemrElementRule(
 *   id = "td",
 *   title = @Translation("Table Data Cell (TD)"),
 *   element = "td",
 * )
 */
class Td implements ThemrElementRuleInterface {

  /**
   * {@inheritDoc}
   */
  public function filterRules() {
    return [];
  }

}
