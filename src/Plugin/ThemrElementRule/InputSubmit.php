<?php

namespace Drupal\themr\Plugin\ThemrElementRule;

use Drupal\themr\PluginInterfaces\ThemrElementRuleInterface;

/**
 * The submit button rule.
 *
 * @ThemrElementRule(
 *   id = "input_submit",
 *   title = @Translation("Submit Button (Input)"),
 *   element = "input",
 * )
 */
class InputSubmit implements ThemrElementRuleInterface {

  /**
   * {@inheritDoc}
   */
  public function filterRules() {
    return [
      'type' => 'submit'
    ];
  }

}
