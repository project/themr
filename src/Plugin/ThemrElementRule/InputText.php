<?php

namespace Drupal\themr\Plugin\ThemrElementRule;

use Drupal\themr\PluginInterfaces\ThemrElementRuleInterface;

/**
 * The text field rule.
 *
 * @ThemrElementRule(
 *   id = "input_text",
 *   title = @Translation("Text Field (Input)"),
 *   element = "input",
 * )
 */
class InputText implements ThemrElementRuleInterface {

  /**
   * {@inheritDoc}
   */
  public function filterRules() {
    return [
      'type' => 'text'
    ];
  }

}
