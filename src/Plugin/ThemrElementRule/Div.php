<?php

namespace Drupal\themr\Plugin\ThemrElementRule;

use Drupal\themr\PluginInterfaces\ThemrElementRuleInterface;

/**
 * The background color rule.
 *
 * @ThemrElementRule(
 *   id = "div",
 *   title = @Translation("DIV"),
 *   element = "div",
 * )
 */
class Div implements ThemrElementRuleInterface {

  /**
   * {@inheritDoc}
   */
  public function filterRules() {
    return [];
  }

}
