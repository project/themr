<?php

namespace Drupal\themr\Plugin\ThemrElementRule;

use Drupal\themr\PluginInterfaces\ThemrElementRuleInterface;

/**
 * The link rule.
 *
 * @ThemrElementRule(
 *   id = "a",
 *   title = @Translation("Link"),
 *   element = "a",
 * )
 */
class A implements ThemrElementRuleInterface {

  /**
   * {@inheritDoc}
   */
  public function filterRules() {
    return [];
  }

}
