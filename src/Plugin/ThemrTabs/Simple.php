<?php

namespace Drupal\themr\Plugin\ThemrTabs;

use Drupal\themr\PluginInterfaces\ThemrTabsInterface;

/**
 * The simple tab.
 *
 * @ThemrTabs(
 *   id = "simple",
 *   title = @Translation("Simple"),
 *   weight = 0
 * )
 */
class Simple implements ThemrTabsInterface {
}
