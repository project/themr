<?php

namespace Drupal\themr\PluginInterfaces;

/**
 * Interface for Themr Suggestion Element Rule modifiers.
 */
interface ThemrSuggestionElementRuleInterface {

  /**
   * Filter rules to check if the class meets the criteria.
   *
   * @return array
   *   The filter rules to add.
   */
  public function filterRules();

  /**
   * Gives back one or many element render suggestions.
   *
   * @return array
   *   The suggestions to add.
   */
  public function suggestions();

}
