<?php

namespace Drupal\themr\PluginInterfaces;

/**
 * Interface for Themr Element Rule modifiers.
 */
interface ThemrElementRuleInterface {

  /**
   * Filter rules to check if the class meets the criteria.
   *
   * @return array
   *   The filter rules to add.
   */
  public function filterRules();

}
