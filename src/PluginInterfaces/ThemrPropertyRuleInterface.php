<?php

namespace Drupal\themr\PluginInterfaces;

use Drupal\Core\Form\FormStateInterface;

/**
 * Interface for Themr Property Rule modifiers.
 */
interface ThemrPropertyRuleInterface {

  /**
   * Gives back one or many form elements.
   *
   * @param array $form
   *   The form up until then.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   *
   * @return array
   *   The elements to add. May only include new keys.
   */
  public function addElement($form, FormStateInterface $form_state);

  /**
   * Gives back a list of elements to exlude for this property.
   *
   * @return array
   *   The elements to exclude.
   */
  public function excludeElements();

  /**
   * Gives back the list of events to trigger this on.
   *
   * @return array
   *   The events to trigger on.
   */
  public function triggerEvents();

  /**
   * Adds a manual widget library.
   *
   * @return array
   *   The libraries.
   */
  public function addLibrary();
}
