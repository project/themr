<?php

namespace Drupal\themr\Annotation;

use Drupal\Component\Annotation\Plugin;
use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * Declare a simple or advanced group for HTML properties.
 *
 * @ingroup themr_property_group
 *
 * @Annotation
 */
class ThemrPropertyGroup extends Plugin {

  // All should be translatable.
  use StringTranslationTrait;

  /**
   * The plugin ID.
   */
  public string $id;

  /**
   * The human-readable title of the group.
   *
   * @var Drupal\Core\Annotation\Translation|string
   *
   * @ingroup plugin_translatable
   */
  public $title;

  /**
   * The weight to show the group.
   */
  public int $weight;

  /**
   * Should the group be opened by default.
   */
  public bool $open;

}
