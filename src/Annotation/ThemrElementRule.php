<?php

namespace Drupal\themr\Annotation;

use Drupal\Component\Annotation\Plugin;
use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * Declare a simple or advanced rule for a HTML element.
 *
 * @ingroup themr_element_rule
 *
 * @Annotation
 */
class ThemrElementRule extends Plugin {

  // All should be translatable.
  use StringTranslationTrait;

  /**
   * The plugin ID.
   */
  public string $id;

  /**
   * The human-readable title of the element (Button).
   *
   * @var Drupal\Core\Annotation\Translation|string
   *
   * @ingroup plugin_translatable
   */
  public $title;

  /**
   * The target element.
   */
  public string $element;

}
