<?php

namespace Drupal\themr\Annotation;

use Drupal\Component\Annotation\Plugin;
use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * Declare a simple or advanced rule for a HTML property.
 *
 * @ingroup themr_property_rule
 *
 * @Annotation
 */
class ThemrPropertyRule extends Plugin {

  // All should be translatable.
  use StringTranslationTrait;

  /**
   * The plugin ID.
   */
  public string $id;

  /**
   * The human-readable title of the plugin.
   *
   * @var Drupal\Core\Annotation\Translation|string
   *
   * @ingroup plugin_translatable
   */
  public $title;

  /**
   * The target css property, can be empty if complex.
   */
  public string $css_property;

  /**
   * The property group.
   */
  public string $group;

  /**
   * The property value prefix, can be empty.
   */
  public string $prefix;

  /**
   * The property value suffix, can be empty.
   */
  public string $suffix;

}
