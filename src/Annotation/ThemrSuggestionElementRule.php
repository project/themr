<?php

namespace Drupal\themr\Annotation;

use Drupal\Component\Annotation\Plugin;
use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * Declare suggestions for a HTML element.
 *
 * @ingroup themr_suggestion_element_rule
 *
 * @Annotation
 */
class ThemrSuggestionElementRule extends Plugin {

  // All should be translatable.
  use StringTranslationTrait;

  /**
   * The plugin ID.
   */
  public string $id;

  /**
   * The human-readable title of the suggestion.
   *
   * @var Drupal\Core\Annotation\Translation|string
   *
   * @ingroup plugin_translatable
   */
  public $title;

  /**
   * The target element.
   */
  public string $element;

}
