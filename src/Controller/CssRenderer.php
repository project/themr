<?php

namespace Drupal\themr\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\HttpFoundation\Response;

/**
 * Outputs the CSS.
 */
class CssRenderer extends ControllerBase {

  /**
   * Output the CSS.
   */
  public function flush() {
    $data = $this->config('themr.css')->getRawData();
    $css = [];
    if (is_array($data)) {
      foreach ($data as $element => $rules) {
        foreach ($rules as $rule) {
          $rawRow = $element;
          if ($rule['id']) {
            $rawRow .= '#' . $rule['id'];
          }
          if ($rule['classes']) {
            $rawRow .= '.' . implode('.', explode(" ", $rule['classes']));
          }
          $rawRow .= " {\n";
          foreach ($rule['css'] as $property => $value) {
            $rawRow .= "  $property: $value;\n";
          }
          $rawRow .= "}\n";
          $css[] = $rawRow;
        }
      }
    }

    $response = new Response(implode("\n", $css));
    $response->headers->set('Content-Type', 'text/css');
    return $response;
  }
}
