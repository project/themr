<?php

/**
 * @file
 * Contains \Drupal\themr\Form\CssEditor.
 */

namespace Drupal\themr\Form;

use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\themr\PluginManager\ThemrPropertyGroupManager;
use Drupal\themr\PluginManager\ThemrPropertyManager;
use Drupal\themr\PluginManager\ThemrTabsManager;
use Symfony\Component\DependencyInjection\ContainerInterface;

class CssEditor extends ConfigFormBase implements ContainerInjectionInterface {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'themr.css',
    ];
  }

  /**
   * Property Rule Manager.
   */
  public ThemrPropertyManager $themrPropertyManager;

  /**
   * Property Group Manager.
   */
  public ThemrPropertyGroupManager $themrPropertyGroupManager;

  /**
   * Tabs manager.
   */
  public ThemrTabsManager $themrTabsManager;

  /**
   * Constructor.
   */
  public function __construct(ThemrPropertyManager $themrPropertyManager, ThemrPropertyGroupManager $themrPropertyGroupManager, ThemrTabsManager $themrTabsManager) {
    $this->themrPropertyManager = $themrPropertyManager;
    $this->themrPropertyGroupManager = $themrPropertyGroupManager;
    $this->themrTabsManager = $themrTabsManager;
  }

  /**
   * {@inheritDoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('plugin.manager.property_manager'),
      $container->get('plugin.manager.property_group_manager'),
      $container->get('plugin.manager.tabs_manager'),
    );
  }
  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'themr_css_editor';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $form['#prefix'] = '<div id="themr-dragbar"></div>';

    $form['selector'] = [
      '#type' => 'details',
      '#title' => $this->t('Selector'),
    ];


    $form['selector']['element_parents'] = [
      '#type' => 'select',
      '#title' => $this->t('With Parent'),
      '#description' => $this->t('If you want it to be styled with a special parent, choose it here.'),
      '#attributes' => [
        'id' => 'element-parents',
      ],
      '#options' => [
        '' => 'none'
      ],
    ];

    $form['tabs_start'] = [
      '#markup' => '<div id="smarttab"><ul class="nav">',
    ];
    foreach ($this->themrTabsManager->getDefinitions() as $definition) {
      $form['tabs'][$definition['id']] = [
        '#markup' => '<li><a class="nav-link" href="#' . $definition['id'] . '">' . $definition['title'] . '</a></li>',
      ];
    }

    $form['#attributes']['class'] = [
      'themr-form',
      'themr-admin',
    ];

    $form['element_type'] = [
      '#type' => 'hidden',
      '#title' => $this->t('Element Type'),
      '#attributes' => [
        'id' => 'element-type',
      ],
    ];

    $form['element_id'] = [
      '#type' => 'hidden',
      '#title' => $this->t('Element Id'),
      '#attributes' => [
        'id' => 'element-id',
      ],
    ];

    $form['element_classes'] = [
      '#type' => 'hidden',
      '#title' => $this->t('Element Class'),
      '#attributes' => [
        'id' => 'element-class',
      ],
    ];

    $form['tabs_content_start'] = [
      '#markup' => "\n\n" . '<div class="tab-content">' . "\n\n",
    ];

    foreach ($this->themrTabsManager->getDefinitions() as $tabsDefinition) {
      $form['tabs_content_' . $tabsDefinition['id'] . '_start'] = [
        '#markup' => '<div id="' . $tabsDefinition['id'] . '" class="tab-pane" role="tabpanel">' . "\n\n",
      ];

      $tabs = [];
      foreach ($this->themrPropertyGroupManager->getDefinitions() as $definition) {
        if ($definition['tab'] == $tabsDefinition['id']) {
          $form[$definition['tab']][$definition['id']] = [
            '#type' => 'details',
            '#title' => $definition['title'],
            '#open' => $definition['open'],
          ];
          $tabs[$definition['id']] = $definition['tab'];
        }
      }

      foreach ($this->themrPropertyManager->getDefinitions() as $definition) {
        if (isset($tabs[$definition['group']])) {
          $instance = $this->themrPropertyManager->createInstance($definition['id']);
          $elements = [];
          $elements = $instance->addElement($form, $form_state);
          $form[$tabs[$definition['group']]][$definition['group']] = array_merge_recursive($elements, $form[$tabs[$definition['group']]][$definition['group']]);
        }
      }

      $form['tabs_content_' . $tabsDefinition['id'] . '_end'] = [
        '#markup' => '</div>',
      ];
    }
    $form['tabs_content_end'] = [
      '#markup' => '</div>',
    ];
    $form['tabs_end'] = [
      '#markup' => '</ul></div>',
    ];

    $form['css_changes'] = [
      '#type' => 'textarea',
      '#attributes' => [
        'id' => 'css-changes',
      ],
    ];

    $form['actions']['#type'] = 'actions';
    $form['actions']['#attributes']['id'] = 'actions-box';
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Save'),
      '#button_type' => 'primary',
    ];

    $form['actions']['cancel'] = [
      '#type' => 'submit',
      '#value' => $this->t('Cancel Changes'),
      '#button_type' => 'secondary',
      '#disabled' => TRUE,
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $formState) {
    $changes = explode("\n", $formState->getValue('css_changes'));
    $store = [];
    foreach ($changes as $change) {
      if ($change && strstr($change, ':')) {
        $rule = explode(':', $change);
        if (empty($rule[1]) && isset($store[$rule[0]])) {
          unset($rule[0]);
        }
        else if (count($rule) == 2) {
          $store[$rule[0]] = trim($rule[1], "\r");
        }
      }
    }
    $css = $this->config('themr.css')->get($formState->getValue('element_type'));
    $id = md5($formState->getValue('element_id') . $formState->getValue('element_classes'));
    $css[$id]['css'] = $store;
    $css[$id]['id'] = $formState->getValue('element_id');
    $css[$id]['classes'] = $formState->getValue('element_classes');
    $this->config('themr.css')->set($formState->getValue('element_type'), $css)->save();
  }
}
