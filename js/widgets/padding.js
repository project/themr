(function ($, Drupal, drupalSettings) {
  $(document).ready(function () {
    $('#padding_per_side').on('change', function () {
      if ($(this).is(':checked')) {
        $('#padding_left').val($('#padding').val());
        $('#padding_right').val($('#padding').val());
        $('#padding_top').val($('#padding').val());
        $('#padding_bottom').val($('#padding').val());
        themrRemoveFromElement('padding');
        themrRerenderElement('padding-left', $('#padding_left').val(), 'rem');
        themrRerenderElement('padding-right', $('#padding_right').val(), 'rem');
        themrRerenderElement('padding-top', $('#padding_top').val(), 'rem');
        themrRerenderElement('padding-bottom', $('#padding_bottom').val(), 'rem');
      }
      else {
        $('#padding').val($('#padding_left').val());
        themrRemoveFromElement('padding-left');
        themrRemoveFromElement('padding-right');
        themrRemoveFromElement('padding-top');
        themrRemoveFromElement('padding-bottom');
        themrRerenderElement('padding', $('#padding').val(), 'rem');
      }
    });
    $('#padding_left').on('input', function() {
      themrRerenderElement('padding-left', $(this).val(), 'rem');
    });
    $('#padding_right').on('input', function () {
      themrRerenderElement('padding-right', $(this).val(), 'rem');
    });
    $('#padding_top').on('input', function () {
      themrRerenderElement('padding-top', $(this).val(), 'rem');
    });
    $('#padding_bottom').on('input', function () {
      themrRerenderElement('padding-bottom', $(this).val(), 'rem');
    });
    $('#padding').on('input', function () {
      themrRerenderElement('padding', $(this).val(), 'rem');
    });
  });
})(jQuery, Drupal, drupalSettings);
