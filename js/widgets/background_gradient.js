(function ($, Drupal, drupalSettings) {
  let amountoOfGradients = 2;
  $(document).ready(function () {
    let markup = "";
    let colorPickerVisible = false;
    for (let i = 0; i < amountoOfGradients; i++) {
      markup += '<div class="themr-color">';
      markup += "<button class=\"themr-color-picker\" id=\"background-gradient-" + i + "\">Color #" + i + "</button>";
      markup += '<div class="themr-color-box">';
      markup += "<button data-jscolor=\"{}\" id=\"background-gradient-fixed-" + i + "\">Fixed Color</button>";
      markup += "<button id=\"background-remove-" + i + "\">Remove Color</button>";
      markup += '</div></div></div>';
    }
    $('#background-gradient-picker').html(markup);
    for (let i = 0; i < amountoOfGradients; i++) {
      let parent = document.querySelector('#background-gradient-fixed-' + i);
      new Picker({
        parent: parent,
        onChange: (color) => {
          let colorString = 'rgba(' + color._rgba[0] + ',' + color._rgba[1] + ',' + color._rgba[2] + ',' + color._rgba[3] + ')';
          $('#background-gradient-' + i).css({'background-color': colorString});
          $('#background-gradient-' + i).data('color-picked', true);
          recalculateGradient();
        },
      });
      $('#background-gradient-' + i).on('click', function () {
        let box = $(this).parent().find('.themr-color-box');
        if (!box.is(':visible')) {
          box.show();
          colorPickerVisible = true;
        } else {
          box.hide();
        }
        return false;
      });
      $('#background-remove-' + i).on('click', function () {
        $('#background-gradient-' + i).css({ 'background-color': '' });
        $('#background-gradient-' + i).data('color-picked', false);
        recalculateGradient();
        return false;
      });
    }
    $('#background-image-gradient-direction').on('change', recalculateGradient);
    $('body').on('click', function(event) {
      if (colorPickerVisible) {
        if (!$(event.target).closest('.themr-color').length) {
          colorPickerVisible = false;
          $('.themr-color-box').hide();
          return false;
        }
      }
    })
  });

  function recalculateGradient() {
    let values = [];
    values.push($('#background-image-gradient-type').val() + '(' + $('#background-image-gradient-direction').val() + 'deg');
    for (let i = 0; i < amountoOfGradients; i++) {
      if ($('#background-gradient-' + i).data('color-picked')) {
        values.push($('#background-gradient-' + i).css('background-color'));
      }
    }
    let realValue = values.length > 1 ? values.join(',') + ')' : '';
    $('#background-gradient-preview').css('background-image', realValue);
    themrRerenderElement('background-image', realValue);
  }
})(jQuery, Drupal, drupalSettings);
