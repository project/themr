/**
 * jQuery is a terrible solution for this, but it's good enough for now.
 *
 * Will be rewritten in Typescript.
 */
let themrAllChanges = [];
(function ($, Drupal, drupalSettings) {
  $(document).ready(function() {
    $('#smarttab').smartTab();
    $('body').append('<button class="themr-btn themr-admin"><img src="/modules/custom/themr/css/themrlogo.png"></button>');
    $('body').append($('#themr-editor'));
    $('.themr-btn').on('click', function() {
      cancelChanges();
      setUndoRedo();
      dragSidebar();
      enableSelectorMode();
      $('#themr-editor').show();
    });
  });

  function enableSelectorMode() {
    // Attach clickable elements everywhere.
    $('html *').each(function() {
      let parents = $(this).parents();
      // Check so it's allowed from the rules.
      if (!$(this).hasClass('themr-admin') && $(this).get(0).nodeName.toLowerCase() in drupalSettings.themr.allowed_elements) {
        $(this).addClass('themr-wrapper').addClass('themr-level-' + parents.length);
      }
    });
    // Remove everything that is admin based.
    $('.themr-admin *').each(function() {
      $(this).removeClass('themr-wrapper').removeClass(function (index, className) {
        return (className.match(/(^|\s)themr-level-\S+/g) || []).join(' ');
      });
    });
    // HiJack right click.
    $('.themr-wrapper').off('contextmenu').on('contextmenu', function (e) {
      $('.themr-selected').removeClass('themr-selected');
      layerSelector(this, e);
      //$(this).addClass('themr-selected');
      return false;
    });

    function selectedElement(selector, element, id, classes) {
      let structure = $(selector).getPath();
      $('#element-type').val(element);
      $('#element-id').val(id);
      $('#element-class').val(classes);
      hydrateValues();
      fixEditor();
      let options = [];
      options.push('<option value="">' + Drupal.t('No Parent'));
      for (let i in structure) {
        if (i > 0) {
          let formatted = themrFormattedValue(structure[i]);
          options.push('<option value=' + i + '>' + formatted);
        }
      }
      $('#element-parents').html(options);
    }

    function layerSelector(that, event) {
      let structure = $(that).getPath();
      if (structure.length > 2) {
        let contextMenu = '<div id="contextMenu" class="dropdown clearfix"><ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu" style="display:block;position:static;">'
        for (let x of structure) {
          let id = typeof x.id == 'undefined' || x.id == 'undefined' ? '' : x.id;
          if (x.element != 'html') {
            let picker = themrFormattedValue({
              element: x.element,
              class: x.class,
              id: id,
            });
            classes = x.class ? x.class.join(' ') : '';
            contextMenu += '<li><a class="themr-element-picker" tabindex="-1" href="#" data-element="' + x.element + '" data-id="' + id + '" data-class="' + classes + '" data-selector="' + picker +'">' + picker + '</a></li >';
          }
        }
        contextMenu += '</ul></div >';
        $('body').append(contextMenu);
        $('#contextMenu').css({
          display: "block",
          left: event.pageX,
          top: event.pageY
        });
        $('.themr-element-picker').on('mouseover', function() {
          $($(this).data('selector')).addClass('themr-level-hovering');
        })

        $('.themr-element-picker').on('mouseout', function () {
          $($(this).data('selector')).removeClass('themr-level-hovering');
        })

        $('.themr-element-picker').on('click', function () {
          $('#actions-box').show();
          $('#smarttab').show();
          selectedElement($(this).data('selector'), $(this).data('element'), $(this).data('id'), $(this).data('class'));
          // Reset the history.
          themrAllChanges = [];
          $('.themr-level-hovering').removeClass('themr-level-hovering');
        })

        $('html').click(function () {
          $('#contextMenu').remove();
        });
      }
    }
  }

  function fixEditor() {
    for (let t in drupalSettings.themr.properties) {
      let property = drupalSettings.themr.properties[t];
      for (let triggerEvent of property.triggerEvents) {
        $(property.selector).off(triggerEvent).on(triggerEvent, function() {
          themrRerenderElement(property.property, $(property.selector).val(), property.suffix);
        });
      }
    }
  }

  function hydrateValues() {
    let styles = $(themrGetFormattedValueFromInput()).attr('style');
    if (typeof styles !== 'undefined' && styles != '') {
      for (let t in drupalSettings.themr.properties) {
        let property = drupalSettings.themr.properties[t];
        for (let part of directStyles.split(';')) {
          part = part.trim();
          console.log(part);
          console.log(property);
        }
      }
    }
  }

  function setUndoRedo() {
    $(document).on('keydown', function (e) {
      if (e.key === 'z' && e.ctrlKey) {
        e.preventDefault();
        revertOneChange();
      }
    });
  }

  function cancelChanges() {
    $('#edit-cancel').on('click', function(e) {
      e.preventDefault();
      revertAllChanges();
    })
  }

  function revertOneChange() {
    if (themrAllChanges.length >= 1) {
      let part = themrAllChanges.pop();
      if (part.oldValue == null) {
        let formatted = themrGetFormattedValueFromInput();
        $(formatted).css(part.property, '');
      }
      else {
        let formatted = themrGetFormattedValueFromInput();
        $(formatted).css(part.property, part.oldValue);
      }
      let newValue = $('#css-changes').val().split("\n").slice(0, -2);
      $('#css-changes').val(newValue.join("\n") + "\n");
    }
    if (themrAllChanges.length == 0) {
      $('#edit-cancel').attr('disabled', 'disabled').addClass('is-disabled');
    }
  }

  function revertAllChanges() {
    for (let i = 0; i <= themrAllChanges.length; i++) {
      revertOneChange();
    }
  }

  function dragSidebar() {
    let dragging = false;
    $('#themr-dragbar').on('mousedown', function (e) {
      e.preventDefault();

      dragging = true;
      $(document).on('mousemove', function (e) {
        $('#themr-editor').css("left", e.pageX + 2).width((($(window).width() - e.pageX)) + 'px');
      });
    });

    $(document).on('mouseup', function (e) {
      if (dragging) {
        $(document).off('mousemove');
        dragging = false;
      }
    });
  }


})(jQuery, Drupal, drupalSettings);

function themrFormattedValue(structure) {
  let formatted = structure['element'];
  formatted += structure['id'] ? '#' + structure['id'] : '';
  formatted += structure['class'] ? '.' + structure['class'].join('.') : '';
  return formatted;
}

function themrGetFormattedValueFromInput() {
  let formatted = themrFormattedValue({
    'element': jQuery('#element-type').val(),
    'id': jQuery('#element-id').val(),
    'class': jQuery('#element-class').val().split(' '),
  })
  return formatted;
}

function themrGetFullElement() {
  return jQuery(themrGetFormattedValueFromInput()).outer();
}

function themrGetComputedStyles() {
  const element = document.querySelector(themrGetFormattedValueFromInput());
  let diffStyles = getStylesWithoutDefaults(element);
  return diffStyles;
}


function getStylesWithoutDefaults(element) {
  // creating an empty dummy object to compare with
  var dummy = document.createElement('element-' + (new Date().getTime()));
  document.body.appendChild(dummy);

  // getting computed styles for both elements
  var defaultStyles = getComputedStyle(dummy);
  var elementStyles = getComputedStyle(element);

  // calculating the difference
  var diff = {};
  for (var key in elementStyles) {
    if (elementStyles.hasOwnProperty(key)
      && defaultStyles[key] !== elementStyles[key]) {
      diff[key] = elementStyles[key];
    }
  }

  // clear dom
  dummy.remove();

  return diff;
}

function themrRerenderElement(cssProperty, value, suffix) {
  jQuery('#edit-cancel').attr('disabled', false).removeClass('is-disabled');
  writeChange('add', cssProperty, value, suffix);
  suffix = typeof suffix == 'undefined' ? '' : suffix;
  let formatted = themrGetFormattedValueFromInput();
  jQuery('#css-changes').val(jQuery('#css-changes').val() + cssProperty + ':' + value + suffix + "\n");
  jQuery(formatted).css(cssProperty, value + suffix);
}

function themrRemoveFromElement(cssProperty) {
  writeChange('delete', cssProperty);
  let formatted = themrGetFormattedValueFromInput();
  jQuery(formatted).css(cssProperty, '');
}

function writeChange(type, cssProperty, value, suffix) {
  let formatted = themrGetFormattedValueFromInput();
  // Check actual values for styles.
  let oldValue = null;
  let directStyles = jQuery(formatted).attr('style');
  if (typeof directStyles !== 'undefined' && directStyles != '') {
    for (let part of directStyles.split(';')) {
      part = part.trim();
      if (part.substring(0, cssProperty.length) == cssProperty) {
        oldValue = part.substring((cssProperty.length + 1)).trim();
      }
    }
  }
  let change = {
    'type': type,
    'oldValue': oldValue,
    'property': cssProperty,
  };
  switch (type) {
    case 'add':
      change['value'] = value;
      change['suffix'] = suffix;
      break;
  }

  themrAllChanges.push(change);
}
