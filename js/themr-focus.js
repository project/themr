(function ($) {
  $.fn.extend({
    themrFocus: function () {
      $(this).each(function () {
        $(this).addClass('themr-selected');
      });
      $('body > *:not(.themr-selected)').addClass('themr-blur');
    }
  });
})(jQuery);
