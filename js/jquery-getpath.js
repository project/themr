/*
  jQuery-GetPath v0.01, by Dave Cardwell. (2007-04-27)

  http://davecardwell.co.uk/javascript/jquery/plugins/jquery-getpath/

  Copyright (c)2007 Dave Cardwell. All rights reserved.
  Released under the MIT License.


  Usage:
  var path = $('#foo').getPath();
*/

jQuery.fn.extend({
  getPath: function (path) {
    // The first time this function is called, path won't be defined.
    if (typeof path == 'undefined') path = [];

    // If this element is <html> we've reached the end of the path.
    if (this.is('html')) {
      path.push({
        'element': 'html',
      });
      return path;
    }

    // Add the element name.
    let part = {
      'element': this.get(0).nodeName.toLowerCase(),
    };

    // Determine the IDs and path.
    let id = this.attr('id');
    let className = this.attr('class');

    // Add the #id if there is one.
    if (typeof id != 'undefined') {
      part['id'] = id;
    }

    // Add any classes.
    if (typeof className != 'undefined') {
      let classes = className.split(/[\s\n]+/);
      let givenClasses = [];
      for (let o of classes) {
        if (o.substr(0, 6) !== 'themr-') {
          givenClasses.push(o);
        }
      }
      part['class'] = givenClasses.length ? givenClasses : null;
    }
    path.push(part);

    // Recurse up the DOM.
    return this.parent().getPath(path);
  }
});

jQuery.fn.outer = function () {
  let outerElement = jQuery(this).clone();
  outerElement.children().remove();
  return outerElement[0].outerHTML;
}
